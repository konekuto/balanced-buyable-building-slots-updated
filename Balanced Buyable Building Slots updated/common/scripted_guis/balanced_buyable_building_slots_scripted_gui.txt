balanced_add_building_slot_gui = {
	scope = character
	saved_scopes = {
		holding
		county
	}	

	is_shown = {
		scope:holding = {
			holder = {
				OR = {
					this = root
					is_vassal_or_below_of = root
				}
			}
			scope:holding.var:extension_made < 3
		}
	}

	is_valid = {
		custom_tooltip = {
			text = balanced_add_building_slot_interaction_40

			trigger_if = {
				limit = {
					scope:holding.var:extension_made = 0
				}
				scope:holding = {
					title_province = {
						NAND = {
							county = { development_level < 40 }
						}
					}
				}
			}
		}
		custom_tooltip = {
			text = balanced_add_building_slot_interaction_60

			trigger_if = {
				limit = {
					scope:holding.var:extension_made = 1
				}
				scope:holding = {
					title_province = {
						NAND = {
							county = { development_level < 60 }
						}
					}
				}
			}
		}
		custom_tooltip = {
			text = balanced_add_building_slot_interaction_75

			trigger_if = {
				limit = {
					scope:holding.var:extension_made = 2
				}
				scope:holding = {
					title_province = {
						NAND = {
							county = { development_level < 75 }
						}
					}
				}
			}
		}
		trigger_if = {
			limit = {
				scope:holding = {
					scope:holding.var:extension_made = 0
				}
			}
			root = { gold >= 500 }
		}
		trigger_if = {
			limit = {
				scope:holding = {
					scope:holding.var:extension_made = 1
				}
			}
			root = { gold >= 700 }
		}
		trigger_if = {
			limit = {
				scope:holding = {
					scope:holding.var:extension_made = 2
				}
			}
			root = { gold >= 1000 }
		}
	}

	effect = {
		if = {
			limit = { 
				scope:holding = {
					scope:holding.var:extension_made = 0
				}
			}
			remove_short_term_gold = 500
		}
		if = {
			limit = {
				scope:holding = {
					scope:holding.var:extension_made = 1
				}
			}
			remove_short_term_gold = 700
		}
		if = {
			limit = {
				scope:holding = {
					scope:holding.var:extension_made = 2
				}
			}
			remove_short_term_gold = 1000
		}
		custom_tooltip = {
			text = balanced_add_building_slot_interaction_effect
			root = {
				send_interface_toast = {
					title = balanced_add_building_slot_interaction_notification
					type = event_toast_effect_good
					left_icon = root
					right_icon = scope:holding
					custom_tooltip = {
						text = balanced_add_building_slot_interaction_notification_tooltip
						scope:holding.title_province = {
							add_province_modifier = extra_building_slot
						}
						scope:holding = {
							change_variable = {
								name = extension_made
								add = 1
							}
							extension_made = {
								add = 1
							}
						}
					}
				}
			}
		}
	}
	
}